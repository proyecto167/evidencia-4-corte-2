/* Creación del archivo app*/

function llenar(){
    var limite=document.getElementById('limite').value;
    var listanumeros=document.getElementById('numeros');
    //removeOptions(listanumeros);
    var arregloal=[];

    // limpiar opciones
    while(listanumeros.options.length>0){
        listanumeros.remove(0);
    }

    // commit generación de números aleatorios
    for(let i=0;i<limite;i++){
        // let aleatorios
        let aleatorios = Math.floor(Math.random() * 50)+1;
        
        //listanumeros.options[i]=new Option(aleatorios,aleatorios);
        arregloal[i]=aleatorios;
    }

    var arregloA=menoramayor(arregloal);

    if(limite==0){
        for(let i=0;i<limite;i++){
            arregloA[i]=0;
        }
    }

    for(let i=0;i<limite;i++){
        listanumeros.options[i]=new Option(arregloA[i],arregloA[i]);
    }

    return arregloA;

    
}

/* Función 1
 Diseña una función que reciba como argumento un arreglo de valores
enteros de 20 posiciones , regrese el valor promedio de los elementos del arreglo.
*/

function promedio(arreglo){
    let suma=0;
    for(let i=0;i<arreglo.length;i++){
        suma=suma+arreglo[i];
    }
    return suma/arreglo.length;
}

// console.log(promedio(arr));

/* Función 2
Diseñe una función que reciba como argumento un arreglo de 20 valores
numéricos enteros, y me regrese la cantidad de valores pares que existe en el
arreglo
*/

function paresimpares(arreglo){
    var limite=document.getElementById('limite').value;
    let pares=0.0;
    let impares=0.0;
    let simetrico="";
    for(let i=0;i<arreglo.length;i++){
        if(arreglo[i]%2==0){
            pares=pares+1;
        } else{
            impares=impares+1;
        }
    }

    
    pares=(pares*100)/arreglo.length;
    impares=(impares*100)/arreglo.length;

    if(Math.abs(pares - impares) > 25){
        simetrico="No es Simétrico";
    } else{
        simetrico="Es Simétrico";
    }

    var porcentajepares=document.getElementById('porPares');
    var porcentajeimpares=document.getElementById('porImpares');
    var sim=document.getElementById('EsSimetrico');

    if(limite!=0){
        porcentajepares.innerHTML=(pares+"%");
        porcentajeimpares.innerHTML=impares+"%";
        sim.innerHTML=simetrico;
    } else {
        porcentajepares.innerHTML=null;
        porcentajeimpares.innerHTML=null;
        sim.innerHTML=null;
    }
    

}

// console.log(pares(arr));

/* Función 3 
Diseñe una función que reciba como argumento un arreglo de 20 valores
numéricos enteros, ordene los valores del arreglo de mayor a menor.
*/

function mayoramenor(arreglo){
    let array=arreglo;
    band=false;

    while(!band){
        band=true;
        for(let i=0;i<array.length;i++){
            if(array[i]<array[i+1]){
                aux=array[i+1];
                array[i+1]=array[i];
                array[i]=aux;
                band=false;
            }
        }
    }
    return array;
}

function menoramayor(arreglo){
    let array=arreglo;
    band=false;

    while(!band){
        band=true;
        for(let i=0;i<array.length;i++){
            if(array[i]>array[i+1]){
                aux=array[i+1];
                array[i+1]=array[i];
                array[i]=aux;
                band=false;
            }
        }
    }
    return array;
}


//console.log(mayoramenor(arr));
/*function mostrar(){
    let arr=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
    p0=document.getElementById('resF0');
    p0.innerHTML=arr;


    p1=document.getElementById('resF1');
    p2=document.getElementById('resF2');
    p3=document.getElementById('resF3');
    let prom=promedio(arr);
    let par=pares(arr);
    let maymen=mayoramenor(arr);
    
    p1.innerHTML=prom;
    p2.innerHTML=par;
    p3.innerHTML=maymen;
}*/

// contar los numeros pares e impares para saber su porcentaje
// la diferencia no sea mayor al 25%